import React, { Component } from 'react';
import { Platform, StyleSheet,  View, ScrollView, FlatList } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import ProfileScreen from './screens/ProfileScreen';
import LoginScreen from './screens/LoginScreen';


const AppNavigator = createStackNavigator({
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      header: null
    }
  },
  Profile: {
    screen: ProfileScreen,
    // navigationBarStyle : {navBarHidden: true },
    navigationOptions: {
      header: null
    }
  }
},{
  initialRouteName: 'Login',
});

export default createAppContainer(AppNavigator);