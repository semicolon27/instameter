import React, { Component } from 'react';
import { Platform, StyleSheet,  View, ScrollView, FlatList } from 'react-native';
import { ThemeProvider, Header, Text, Avatar, Image, Icon, ListItem } from 'react-native-elements';
import { AppLoading } from "expo";
import Axios from 'axios';

import * as Font from "expo-font";

const theme = {
  color: '#1976D3'
}
const data = {
  name: "Jack",
  desc: "Tukang Nasi Padang",
  profilePic: require("../assets/img/default.jpg"),
  followers: 12100,
  following: 1111,
  sumPost: 120,
  photos: [
    require("../assets/img/10-10.jpg"),
    require("../assets/img/10-11.jpg"),
    require("../assets/img/10-13.jpg"),
    require("../assets/img/10-14-Day.jpg"),
    require("../assets/img/10-14-Night.jpg"),
    require("../assets/img/10-15-beta-dark.jpg"),
    require("../assets/img/10-15-beta-light.jpg"),
  ]
}
const numFormatter = (num) => {
  try{
    // let a = Math.abs(num)
    // switch(a){
    //   case a > 999999999999 :
    //     return Math.sign(num)*((Math.abs(num)/1000000000000).toFixed(1)) + 'T'
    //     break;
    //   case a > 999999999 :
    //     return Math.sign(num)*((Math.abs(num)/1000000000).toFixed(1)) + 'B'
    //     break;
    //   case a > 999999 :
    //     return Math.sign(num)*((Math.abs(num)/1000000).toFixed(1)) + 'M'
    //     break;
    //   case a > 999 :
    //     return Math.sign(num)*((Math.abs(num)/1000).toFixed(1)) + 'K'
    //     break;
    //   default: 
    //     return Math.sign(num)*Math.abs(num)
    //     break;
    //   }
      return Math.abs(num) > 999999 ? Math.sign(num)*((Math.abs(num)/1000000).toFixed(1)) + 'M' : Math.abs(num) > 999 ? Math.sign(num)*((Math.abs(num)/1000).toFixed(1)) + 'K' : Math.sign(num)*Math.abs(num)
  }catch(err){
    return "Invalid Value Type"
  }
}
const urlApi = 'http://10.10.24.27:7007'

export default class ProfileScreen extends Component {
  state = {
    fontLoaded: false,
    dataProfile: {},
    statusLogin : '',
    err: ''
  };
  async componentWillMount() {
    await Font.loadAsync({
      PoppinsLight: require("../assets/fonts/poppins/Poppins-Light.ttf"),
      PoppinsMedium: require("../assets/fonts/poppins/Poppins-Medium.ttf"),
      PoppinsBold: require("../assets/fonts/poppins/Poppins-Bold.ttf"),
      PoppinsRegular: require("../assets/fonts/poppins/Poppins-Regular.ttf"),
    });
    this.setState({ fontLoaded: true });
  }
  async componentDidMount(){
    try {
      let res = await Axios.get(urlApi + "/api/islogin");
      let profile = await Axios.get(urlApi + "/api/instameter/getprofile/"+res.data.user)
      this.setState({ statusLogin: res.data.islogin, dataProfile: profile.data.data[0] })
      console.log(profile.data.data[0])
    } catch (err) {
      this.setState({ err: err.message })
    }
  }
  render() {
    const data = this.state.dataProfile
    console.log(data.username)
    return (
      this.state.fontLoaded ? (
      <ThemeProvider>
        <NavbarComponent />
        <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between' }}>
          <HeaderComponent pp={data.profilePic} user={data.username} status={data.status} />
          <StatusComponent sumPost={data.posts} followers={data.followers} following={data.following} />
          <GalleryComponent />
          <PostComponent user={data.username} />
        </ScrollView>
      </ThemeProvider>
      ) : <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}><Text style={{fontSize: 32}}>Tunggu</Text></View>
    );
  }
}

const NavbarComponent = () => {
  return <Header backgroundColor={theme.color} containerStyle={{ borderBottomColor:'transparent' ,borderBottomWidth:0, shadowColor:'transparent' }} placement="left" leftComponent={{icon: 'menu', color:'#fff'}} centerComponent={{text: 'Profile', style: {color: '#fff', fontSize: 18, fontFamily: 'PoppinsRegular'} }} rightComponent={ <MenuKanan /> } />
}

const HeaderComponent = props => {
  return (
    <>
          <View style={{flex: 4, backgroundColor: theme.color, marginTop: 0}}>
            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                <View style={{flex: 2}}>
                  <Avatar size={100} avatarStyle={{ borderWidth: 3, borderColor: 'white', borderRadius: 100, overflow: 'hidden' }} title='Avatar' titleStyle={{fontSize: 11}} rounded source={ props.pp === 'default.jpg' ? require("../assets/img/default.jpg") : {url: urlApi + '/assets/instameter/img/profile/' + props.pp }} />
                </View>
                <View style={{flex: 1, paddingBottom: 20}}>
                  <Text style={{color: '#fff', textAlign: 'center', fontWeight: '400', fontSize: 20 , paddingTop: 11, fontFamily: 'PoppinsMedium'}}>{props.user}</Text>
                  <Text style={{color: '#fff', textAlign: 'center', fontWeight: '100', fontSize: 11, fontFamily: 'PoppinsLight'}}>{props.status}</Text>
                </View>
            </View>
          </View>
      </>
  )
}

const StatusComponent = props => {
  return (
    <View style={{flex:2, paddingTop: 15}}>
      {/* <View style={{flex:1}}></View> */}
      <View style={{flex: 2, borderBottomWidth: 0.5, borderBottomColor: '#E9E9E9', flexDirection: 'row', justifyContent: 'space-between' , paddingLeft: '10%', paddingRight: '10%'}}>
        <View style={{flexDirection: 'column',marginBottom: 15, justifyContent: 'space-between'}}>
          <View style={{flex:1}}></View>
          <Text style={{ textAlign: 'center', }}>{numFormatter(props.sumPost)}</Text>
          <Text style={{ opacity: 0.6, fontFamily: 'PoppinsRegular',textAlign: 'center', }}>Posts</Text>
          <View style={{flex:1}}></View>
        </View>
        <View style={{marginBottom: 15}}>
          <View style={{flex:1}}></View>
          <Text style={{ textAlign: 'center', }}>{numFormatter(props.followers)}</Text>
          <Text style={{ opacity: 0.6, fontFamily: 'PoppinsRegular',textAlign: 'center'}}>Followers</Text>
          <View style={{flex:1}}></View>
        </View>
        <View style={{marginBottom: 15}}>
          <View style={{flex:1}}></View>
          <Text style={{ textAlign: 'center', }}>{numFormatter(props.following)}</Text>
          <Text style={{ opacity: 0.6, fontFamily: 'PoppinsRegular',textAlign: 'center', }}>Following</Text>
          <View style={{flex:1}}></View>
        </View>
      </View>
      {/* <View style={{flex:1}}></View> */}
    </View>
  )
}

const GalleryComponent = () => {
  return (
    <View style={{flex: 2, flexDirection: 'column', paddingRight: 12, marginBottom: 15 }}>
      <View style={{flex: 1, paddingTop: 10, paddingLeft: 12}}>
        <Text style={{fontFamily: 'PoppinsMedium', marginBottom: 12, fontSize: 15}}>Photos</Text>
      </View>
      <View style={{flex: 4, flexDirection: 'row', paddingLeft: 12, overflow: "scroll"}}>
        <FlatList
        horizontal={true}
        data={data.photos}
        renderItem={({ item }) => (
          <Image source={item} style={{height: 80, width: 80, marginRight: 10}} />
        )}
        keyExtractor={(item, index) => index.toString()}
      />
      </View>
    </View>
  )
}

const PostComponent = props => {
  const username = props.user[0];
  console.log(username)
  return (
    <View style={{flex: 5}}>
      <View style={{flex: 0.1, justifyContent: 'center', marginTop: 10, marginLeft: 12}}>
        <Text style={{fontFamily: 'PoppinsMedium', fontSize:15, marginBottom: -5}}>Post</Text>
      </View>
      <View style={{flex: 0.5}}>
        <ListItem titleStyle={{fontFamily: 'PoppinsRegular'}} leftAvatar={{ title: data.name[0], source: data.profilePic, fontFamily: 'PoppinsRegular', rounded: false, size: 60}} title={props.user} subtitle={<SubtitleComponent />} chevron />
      </View>
      <View style={{flex: 4, margin: 12, marginTop: 0}}>
        <Image source={data.photos[0]} style={{height: 300}} />
      </View>
    </View>
  )
}

const MenuKanan = () => {
  return (
    <View style={{flex: 1, flexDirection: 'row', marginTop: 10}}>
     {/* <Ionicons name='more' color='#fff' /> */}
     <Icon name='search' color='#fff' iconStyle={{marginRight: 10}} />
     <Icon name='more-vert' color='#fff' />
    </View>
  )
}

const SubtitleComponent = () => {
  return (
    <View style={{flex: 1, flexDirection: 'row'}}>
      <View style={{flex: 2}}>
        <Icon name='date-range' size={20} color='#9F9F9F' />
      </View>
      <View style={{flex: 17}}>
        <Text style={{color: '#9F9F9F'}}>25 minutes ago</Text>
      </View>
    </View>
  )
}