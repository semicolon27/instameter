import React from 'react'
import { Text, View, TouchableHighlight, KeyboardAvoidingView } from 'react-native'
import { Input, Image, Avatar } from 'react-native-elements'
import Axios from 'axios';

export default LoginScreen = props => {
  const [user, setUser] = React.useState('')
  const [pass, setPass] = React.useState('')
  const [isLoading, setLoading] = React.useState(false)
  const [err, setErr] = React.useState(false)
  const handleLogin = async () => {
    setLoading(true)
    let res = await Axios.post("http://10.10.24.27:7007/api/instameter/login", {username : user, password: pass});
    setLoading(false)
    if(res.data.status === "loggedin"){
      props.navigation.navigate('Profile')
      setErr(false)
    }else{
      setErr(true)
    }
  }
  return (
    <KeyboardAvoidingView style={styles.container} behavior='padding' enabled>
      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
          <View style={{flex: 2, justifyContent: 'flex-end', padding: 10}}>
            <Image containerStyle={{padding: 10, height: 180, width: 150}} resizeMethod="resize" source={require('../assets/img/login.png')} />
          </View>
          <View style={{flex: 1, justifyContent: 'flex-start', padding: 10}}>
            <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 20, color: !err ? '#00ff21' : 'red'}}>{err === true ? 'Username atau/dan Password Salah': 'Welcome To Instameter'}</Text>
            <Text style={{textAlign: 'center'}}>Login to Continue</Text>
          </View>
      </View>
      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'flex-end', paddingBottom: 30}}>
          <Input onChangeText={v => setUser(v)} placeholder='Username' inputContainerStyle={{marginBottom: 10, borderBottomColor: '#00ff21', opacity: 0.5}} placeholderTextColor='#00ff21' leftIcon={{ name: 'email', color:'#00ff21', paddingRight: 10 }} />
          <Input onChangeText={v => setPass(v)} placeholder='Password' inputContainerStyle={{marginBottom: 10, borderBottomColor: '#00ff21', opacity: 0.5}} placeholderTextColor='#00ff21' leftIcon={{ name: 'lock', color:'#00ff21', paddingRight: 10 }} secureTextEntry={true} />
        <TouchableHighlight style={styles.button} onPress={handleLogin}>
         <Text style={{fontSize:15, color: 'white'}}>{isLoading ? 'Loading gan...' : 'Login'}</Text>
        </TouchableHighlight>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{fontSize: 11}}>Signup for login ?</Text>
        </View>
      </View>
    </KeyboardAvoidingView>
  )
} 

const styles = {
  container: {
    padding: 10,
    paddingTop: 50,
    flex: 1,
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#00ff21',
    padding: 10,
    borderRadius: 50,
    margin: 10,
    
  },
  countContainer: {
    alignItems: 'center',
    padding: 10
  },
  countText: {
    color: '#FF00FF'
  }
}
